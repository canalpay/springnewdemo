package com.example.pjos;

/**
 * Created by CAN-Windows on 9.07.2016.
 */

import org.springframework.data.annotation.Id;


public class Deneme {
    @Id
    private String id;
    private String ad;
    public Deneme(String ad) {
        this.ad = ad;
    }

    public String getAd() {
        return ad;
    }
    @Override
    public String toString(){
        return ad;
    }
}
